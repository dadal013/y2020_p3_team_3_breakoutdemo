# Reflection #

Dylan Adal, Max Liu, Adrian Liu, 4/24/2020, Period 3

This program took us around 2 hours to complete.

Overall, this program was pretty straightforward. Reading through the slides, as well as the documentation 
was pretty helpful, and walked you through most of the steps for doing it. Something we struggled with 
as a group was working together, since we never found a time to do a zoom together. We still managed to 
finish this on time, but we probably could have saved time seeing each other work, so we can work on doing that
next time.