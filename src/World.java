import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

public abstract class World extends Pane {

	private AnimationTimer timer;
	private HashSet<String> tem;
	private boolean gameOver = false;

	public World() {
		tem = new HashSet<String>();
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				getWorld().act(now);
				List<Node> nemu = getWorld().getChildren();
				for (Node i : nemu) {
					if(i instanceof Actor) {
						((Actor) i).act(now);
					}
				}
			}
		};
	}
	
	public void addCode(String co) {
		tem.add(co);
	}
	
	public void removeCode(String co) {
		tem.remove(co);
	}
	
	public boolean isIn(String co) {
		for(String te : tem) {
			if(co.equals(te)) {
				return true;
			}
		}
		return false;
	}

	abstract void act(long now);

	public void add(Actor actor) {
		this.getChildren().add(actor);
	}

	/**
	 * Returns a list of all the actors in the world of the given class.
	 * Implementation directions: The method header is given to you already so just
	 * copy it as is. What you need to do is to first make some kind of List (for
	 * example: ArrayList) of type A objects. Then iterate through that list and if
	 * all of the following conditions are met, then add the Actor to the list and
	 * return the list after the loop is finished.
	 * 
	 * - The Actor's Class type matches the method parameter's Class type Type
	 * Parameters: A - the type of actors Parameters: cls - The type of actor that
	 * will be in the list Returns: a list of all the actors in the world of the
	 * given class.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <A extends Actor> List<A> getObjects(Class<A> cls) {
		List chubby = new ArrayList();
		List<Node> fat = this.getChildren();
		for (Node dyld : fat) {
			if (cls.isInstance(dyld)) {
				chubby.add(dyld);
			}
			
		}

		return chubby;
	
	}



	public void remove(Actor actor) {
		this.getChildren().remove(actor);
	}

	public void start() {
		timer.start();
	}

	public void stop() {
		timer.stop();
	}

	public World getWorld() {
		return this;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

}
