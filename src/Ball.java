import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javafx.scene.image.Image;

public class Ball extends Actor{
	/*
	 * When the Ball hits the bottom of the World, reduce the Score by 1000 points
Note:  To get the Score from the Ball class, use getWorld() and typecast it to a BallWorld.  Then you can call getScore() on that.
When the Ball hits a Brick, add 100 points to the Score

	 */
	private boolean touchPaddle;
	private boolean gameStarted = false;
	private double dx;
	private double dy;
	public Ball() {
		dx = 5;
		dy = 5;
		String path = getClass().getClassLoader().getResource("resources/ball.png").toString();
		Image img = new Image(path);

		this.setImage(img);
	}
	@Override
	void act(long now) {
		// TODO Auto-generated method stub
		move(dx, dy);
		if(this.getX() <= 0) {
			dx = -dx;
		}
		if(this.getY()  <= 0) {
			dy = -dy;
		}
		if(this.getX() + this.getWidth()>= this.getWorld().getWidth()) {
			dx = -dx;
		}
		if(this.getY() + this.getHeight() >= this.getWorld().getHeight()) {
			dy = -dy;
			
			
			if(gameStarted) {
				
				int score = ((BallWorld) (this.getWorld())).getScore().getScore();
				((BallWorld) (this.getWorld())).getScore().setScore(score-1000);
		
				
			}
			
		}

		if(this.getOneIntersectingObject(Paddle.class)!=null) {	
			if(!touchPaddle) { // flag is false so execute bounce code
				touchPaddle = true;
				Paddle paddle = this.getOneIntersectingObject(Paddle.class);
				double ballX = this.getX();
				double lBound = paddle.getX() - paddle.getImage().getWidth() / 2;
				double firstThird = lBound + paddle.getImage().getWidth() / 3;
				double secondThird = firstThird + paddle.getImage().getWidth() / 3;
				double rBound = paddle.getX() + paddle.getImage().getWidth() / 2; 
				if(paddle.getMoving().equals("none")) {  // Paddle is not moving
					// Ball is in the left and right bounds so bounce normally
					if(ballX >= lBound && ballX <= rBound) {
						dy=-dy;
						if(dx < 0) {
							dx = -5;
						}else {
							dx = 5;
						}
					}else if(ballX < lBound) { // Outside left bound
						// using trig to make the angle 30 degrees 
						dy = -5;	
						dx = -8.660254;
					}else { // Outside right bound
						dy = -5;	
						dx = 8.660254;
					}
				}else if(paddle.getMoving().equals("left")){  // Paddle bouncing left
					// Ball is in the middle third region
					if(ballX >= firstThird && ballX <= secondThird) {
						dy = -dy;
						if(dx < 0) {
							dx = -5;
						}else {
							dx = 5;
						}
					}else if(ballX >= lBound && ballX <= firstThird) { // Ball is in the left third region
						dy = -dy;
						if(dx > 0) { // Makes ball move left no matter what
							dx = -5;
						}
					}else if(ballX < lBound) {
						dy = -5;	
						dx = -8.660254;
					}else if(ballX > rBound) {
						dy = -5;	
						dx = 8.66025404;
					}else {
						dy = -dy;
						if(dx < 0) {
							dx = -5;
						}else {
							dx = 5;
						}
					}
				}else { // Paddle bouncing right
					// Ball is in the middle third region
					if(ballX >= firstThird && ballX <= secondThird) {
						dy = -dy;
						if(dx < 0) {
							dx = -5;
						}else {
							dx = 5;
						}
					}else if(ballX <= rBound && ballX >= secondThird) { // Ball is in the left third region
						dy = -dy;
						if(dx < 0) { // Makes ball move right no matter what
							dx = 5;
						}
					}else if(ballX < lBound) {
						dy = -5;	
						dx = -8.660254;
					}else if(ballX > rBound) {
						dy = -5;	
						dx = 8.660254;

						
					}else {
						dy = -dy;
						if(dx < 0) {
							dx = -5;
						}else {
							dx = 5;
						}
					}
					
				}
				
			}
			
		}else {
			touchPaddle = false;
		}

		if(this.getOneIntersectingObject(Brick.class)!=null) {
		int score = ((BallWorld) (this.getWorld())).getScore().getScore();
		((BallWorld) (this.getWorld())).getScore().setScore(score+100);
			
		Brick brack = this.getOneIntersectingObject(Brick.class);
			if(this.getX()<brack.getX()+brack.getImage().getWidth()/2 &&
				this.getX()>brack.getX()-brack.getImage().getWidth()/2) {
				dy = -dy;
			}
			else if(this.getY()<brack.getY()+brack.getImage().getHeight()/2 &&
					this.getY()>brack.getY()-brack.getImage().getHeight()/2) {
				dx = -dx;
			}else {
				dy = -dy;
				dx = -dx;
			}
			getWorld().remove(brack);
			if(this.getWorld().getObjects(Brick.class).size() == 0) { // no bricks left
				String one = "" + ((BallWorld) (this.getWorld())).getScore().getScore() + "\n";
				
					FileWriter out;
					try{
						
					      out = new FileWriter("scores.txt", true);
					      out.write(one, 0, one.length());
					      out.close();
					}catch(IOException i){
					      System.out.println("Error: " + i.getMessage());
					}
				
				
				
				
				this.getWorld().stop();
				this.getWorld().setGameOver(true);
				
				
			}
		}
		

	}
	public void setStart(boolean f) {
		gameStarted = f;
	}

}
