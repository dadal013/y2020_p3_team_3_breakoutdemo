import java.util.ArrayList;

import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Score extends Text{
	
	private int score;
	public Score() {
		score = 0;
		this.setFont(new Font("Serif", 12));
		updateDisplay();
	}
	public void updateDisplay() {
		this.setText(String.valueOf(score));
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int newScore) {
		score = newScore;
		updateDisplay();
	}
}
