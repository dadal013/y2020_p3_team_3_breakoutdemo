import java.util.ArrayList;

public class BallWorld extends World{
	private Score score;

	public BallWorld() {
		score = new Score();
		score.setX(20);
		score.setY(20);
		getChildren().add(score);
	}
	
	public Score getScore() {
		return score;
	}
	
	public void setScore(Score s) {
		score = s;
	}
	
	@Override
	void act(long now) {
		// TODO Auto-generated method stub
		
	}

}
