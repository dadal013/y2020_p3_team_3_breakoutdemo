

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Game extends Application{
	private int helpCounter = 0;
	private int dyldCounter = 0;
	private final int WORLDWIDTH = 1000;
	private final int WORLDHEIGHT = 500;
	private Ball ball = new Ball();
	private Paddle pad = new Paddle();
	private ArrayList<Integer> scores = new ArrayList<>();
	private BallWorld wrd = new BallWorld();
	private BorderPane border;
	private boolean isReplay = false;
/*
 * sets the Title of the Stage
creates a root Node using BorderPane
creates a new BallWorld and sets its preferred size
sets the BallWorld as the center of the root BorderPane
creates a new Scene using the BorderPane root made earlier
creates a Ball, sets its x and y position
adds the Ball to the world
calls start() on the world 
 */
	
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		wrd = new BallWorld();
		int cou = 0;
		Scanner in;
		try{
		     in = new Scanner(new File("scores.txt"));
		     while(in.hasNextInt()) {
		    	 scores.add(in.nextInt());
		    	 cou++;
		     }
		}catch(IOException i){
		     System.out.println("Error: " + i.getMessage());
		}
		insertionSort(scores);
		
		// TODO Auto-generated method stub
		//Group root = new Group();
		primaryStage.setTitle("Ball");
		primaryStage.setResizable(false);
		primaryStage.sizeToScene();
		
		BorderPane bord = new BorderPane();
		Scene title = new Scene(bord);
		
		Text txtfield = new Text("BRICK BREAKER");
		
		Text tet = new Text(10,50, "This is how to play the game.");
		tet.setText("You must bounce the ball off the paddle at the top. \nYou can move the paddle using arrow right and left or by mouse. \nYou gain points by hitting the brick, you lose points if the ball hits the top boundary. \n"
				+ "You win if you destroy all the bricks. You can't lose because in America everyone wins!");
		ToggleGroup grou = new ToggleGroup();
		
		grou.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
		    

			@Override
			public void changed(ObservableValue<? extends Toggle> arg0, Toggle arg1, Toggle arg2) {
				// TODO Auto-generated method stub
				if(helpCounter % 2 == 0) {
					bord.setTop(tet);
				}else {
					bord.setTop(null);
				}
				helpCounter++;
				
			}
		    });
		
		ToggleButton help = new ToggleButton("Help");
		help.setToggleGroup(grou);
		bord.setRight(help);
		bord.setTop(txtfield);
		Text dyldChubby = new Text(10,50, "These are your top 5 scores");
		String dyldText = "";
		for(int dyldc = 0; dyldc < scores.size(); dyldc++) {
			if(dyldc == 5) break;
			dyldText += scores.get(dyldc) + "\n";
		}
		dyldChubby.setText(dyldText);
		ToggleGroup dyldGroup = new ToggleGroup();
		
		dyldGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
		     

			@Override
			public void changed(ObservableValue<? extends Toggle> arg0, Toggle arg1, Toggle arg2) {
				// TODO Auto-generated method stub
				if(dyldCounter % 2 == 0) {
					bord.setTop(dyldChubby);
				}else {
					bord.setTop(null);
				}
				dyldCounter++;
				
			}
		    });
		
		ToggleButton dyld = new ToggleButton("Scores");
		dyld.setToggleGroup(dyldGroup);
		bord.setLeft(dyld);
		
		
		border = new BorderPane();
		Scene scene = new Scene(border);
		BorderPane border2 = new BorderPane();
		Scene gameover = new Scene(border2);
		Text gmover = new Text("GAME OVER");
		border2.setCenter(gmover);
		border2.setPrefSize(1000, 500);
		
		Button replay = new Button("REPLAY");
		Button mainMenu = new Button("Main Menu");
		
		mainMenu.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	primaryStage.setScene(scene);
		        ball.setStart(false);
		        ball.getWorld().setGameOver(false);
		        isReplay = false;
		       try {
				start(primaryStage);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		     
		    }
		});
		
		replay.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	primaryStage.setScene(scene);
		        ball.setStart(false);
		        ball.getWorld().setGameOver(false);
		        isReplay = true;
		      
		       try {
				start(primaryStage);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		        
		    }
		});
		border2.setTop(replay);
		border2.setBottom(mainMenu);
		
		Button start = new Button("Start");
		bord.setPrefSize(1000, 500);
		bord.setCenter(start);
		
		
		
		
		ball.setX(20.0);
		ball.setY(20.0);
		
		start.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        primaryStage.setScene(scene);
		        ball.setStart(true);
		        
		     
		    }
		});
		pad = new Paddle();
		pad.setX(WORLDWIDTH / 2);
		pad.setY((WORLDHEIGHT * 4) / 4.5);
		ArrayList<Brick> brickList = new ArrayList<>();
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 10; j++) {
				Brick brick = new Brick();
				double width = brick.getImage().getWidth();
				double height = brick.getImage().getHeight();
				brick.setX(((WORLDWIDTH * (j + 1)) / width) + WORLDWIDTH / 3);
				brick.setY((WORLDWIDTH * (i + 1)) / height - WORLDHEIGHT / 10);
				brickList.add(brick);
			}
		}
		
		wrd = new BallWorld();
		
		wrd.setPrefSize(WORLDWIDTH, WORLDHEIGHT);
		wrd.add(pad);
		wrd.add(ball);
		
		for(Brick brick : brickList) {
			wrd.add(brick);
		}
		
		border.setCenter(wrd);
		wrd.start();
		primaryStage.show();
		
		wrd.setOnMouseMoved(new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent event) {	
				double xVal = pad.getX();
				if(!ball.getWorld().isGameOver()) {
					pad.setX(event.getX()-pad.getImage().getWidth()/2);
					if(xVal > event.getX()-pad.getImage().getWidth()/2) {
						pad.setMoving("left");
					}else if(xVal < event.getX()-pad.getImage().getWidth()/2) {
						pad.setMoving("right");
					}else {
						pad.setMoving("none");
					}
					pad.setMouseEvent(true);
				}else {
					primaryStage.setScene(gameover);
				}
				
				
			}});
		wrd.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if(event.getCode()==KeyCode.LEFT && !wrd.isIn("LEFT")) {
					wrd.addCode("LEFT");
				}
				if(event.getCode()==KeyCode.RIGHT && !wrd.isIn("RIGHT")) {
					wrd.addCode("RIGHT");
				}
			}
		});
		wrd.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if(event.getCode()==KeyCode.LEFT) {
					wrd.removeCode("LEFT");
				}
				if(event.getCode()==KeyCode.RIGHT) {
					wrd.removeCode("RIGHT");
				}
			}
		});

		
		//root.getChildren().add(border);
		if(isReplay == false) {
			primaryStage.setScene(title);
		}else {
			primaryStage.setScene(scene);
		}
		//primaryStage.setScene(scene);
		primaryStage.show();
		wrd.requestFocus();
		
		
	}
	public void insertionSort(ArrayList<Integer> scores2) {
		// replace these lines with your code

		for (int i = 1; i < scores2.size(); i++) {

			int p = i;
	
			Integer f = scores2.get(i);

			while (p > 0 && (scores2.get(p - 1).compareTo(f) < 0)) {
	
				p--;
			}
	
			scores2.add(p, scores2.remove(i));
		
		}
	}
	
	

}
