import javafx.scene.image.Image;

public class Paddle extends Actor{
	private String moving = "none";
	private boolean mouseEvent = false;

	public Paddle() {
		String path = getClass().getClassLoader().getResource("resources/paddle.png").toString();
		Image img = new Image(path);

		this.setImage(img);
	}
	
	@Override
	void act(long now) {
		if(!this.getWorld().isGameOver()) {
			// TODO Auto-generated method stub
			if(getWorld().isIn("LEFT")){
				this.setX(this.getX()-15);
				setMoving("left");
			}else if(getWorld().isIn("RIGHT")){
				this.setX(this.getX()+15);
				setMoving("right");
			}else {
				if(!mouseEvent) {
					setMoving("none");
				}
			}
			mouseEvent = false;
		}
		
	}

	public String getMoving() {
		return moving;
	}

	public void setMoving(String moving) {
		this.moving = moving;
	}

	public boolean isMouseEvent() {
		return mouseEvent;
	}

	public void setMouseEvent(boolean mouseEvent) {
		this.mouseEvent = mouseEvent;
	}



}
