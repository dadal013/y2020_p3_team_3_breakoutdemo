import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.ImageView;

public abstract class Actor extends ImageView {
	
	World world = (World)(this.getParent());

	abstract void act(long now);

	public void move(double dx, double dy) {
		this.setX(this.getX() + dx);
		this.setY(this.getY() + dy);
	}

	public World getWorld() {
		return (World) (this.getParent());
	}

	public double getWidth() {
		return this.getBoundsInParent().getWidth();
	}

	public double getHeight() {
		return this.getBoundsInParent().getHeight();
	}
	
	public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls) {
		ArrayList<A> list = new ArrayList<>();
		List<A> l = getWorld().getObjects(cls);
		for(A chubby : l) {
			if(chubby != this && chubby.intersects(this.getLayoutBounds())){
				list.add(chubby);
			}
		}
		return list;
	}
	
	public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls) {
		if(this.getIntersectingObjects(cls).size() != 0) return this.getIntersectingObjects(cls).get(0);
		return null;
	}
}
